#include <opencv2/highgui/highgui.hpp>
#include <opencv2/opencv.hpp>
#include <iostream>
#include<fstream>
#include <math.h>

using namespace cv;
using namespace std;


const std::string detLinesWinName = "detected lines";
const std::string trackbarThresh = "Threshold";

const double theta_max = 170 * CV_PI / 180;
const double theta_min = 130 * CV_PI / 180;

const int rho = 3;									//HoughLines parameter
const int theta = 1;								//HoughLines parameter 
const double thetaHough = theta * CV_PI / 180;		//HoughLines parameter

const int toleranceDisp = 5;						//warto�� o jak� mo�e by� �le obliczona warto�� piksela pod�o�a w obrazie dysparycji

struct HoughTresh
{
	int sr;
	int max;
	int min;
};

void checkFile(Mat&, int&, int&);
bool varTheta(const double&);
void findOneLine(Mat, vector<Vec2f>&, vector<Vec2f>&, HoughTresh&);

int main(int argc, char** argv)
{
	int n_row;
	int n_col;

	//string nazwa = "_0001125.png";
	//string nazwa = "_0001517.png";
	//string nazwa = "_0002300.png";
	//string nazwa = "_0002679.png";
	//string nazwa = "car.png";
	string nazwa = "patio.jpg";
	//string nazwa = "room.png";
	

	//-----   Wczytywanie pliku ----------------------------------------------------------
	//------------------------------------------------------------------------------------

	//-----   Read the file   ------------------------------------------------------------
	Mat image = imread(nazwa, CV_LOAD_IMAGE_GRAYSCALE);
	//-----   Check for invalid input   --------------------------------------------------
    if (image.empty()) {cout << "Nie mozna otworzyc obrazu. Sprawdz nazwe pliku." << std::endl; return -1;}
	//-----   jezeli obraz jest wymiaru 2560x720 jest brana tylko prawa polowa obrazu do analizy
	checkFile(image, n_row, n_col);	
	//------------------------------------------------------------------------------------

	

	//-----   Liczenie UV Disparity   ----------------------------------------------------
	//------------------------------------------------------------------------------------

	//-----   Tworzenie macierzy histogram�w UV Disparity   ------------------------------
	Mat U_disp = Mat::zeros(256, n_col, CV_8UC1);
	Mat V_disp = Mat::zeros(n_row, 256, CV_8UC1);
	//-----   Obliczanie UV Disparity   --------------------------------------------------
	for (int row = 0; row < n_row; row++) {
		for (int col = 0; col < n_col; col++) {
			U_disp.Mat::at<uchar>(image.at<uchar>(row, col), col)++;
			V_disp.at<uchar>(row, image.at<uchar>(row, col))++;
		}
	}
	//-----------------------------------------------------------------------------------

	

	//-----   Zapis do pliku    ---------------------------------------------------------
	vector<int> compression_params;
	compression_params.push_back(CV_IMWRITE_PNG_COMPRESSION);
	compression_params.push_back(9);
	string nazwa2 = "U_" + nazwa;
	imwrite(nazwa2, U_disp, compression_params);
	nazwa2 = "V_" + nazwa;
	imwrite(nazwa2, V_disp, compression_params);

	//-----------------------------------------------------------------------------------


	//----------------------wy�wietlanie------------------------------------------------- 
	namedWindow("Display window", WINDOW_AUTOSIZE);// Create a window for display.
	imshow("Display window", image);                   // Show our image inside it.

	namedWindow("U disparity", WINDOW_AUTOSIZE);// Create a window for display.
	imshow("U disparity", U_disp);

	namedWindow("V disparity", WINDOW_AUTOSIZE);// Create a window for display.
	imshow("V disparity", V_disp);
	//-----------------------------------------------------------------------------------
	
	//-----------------------------------------------------------------------------------
	//threshold trackbar
	//-----------------------------------------------------------------------------------


	Scalar srednia, odchylenieStd;
	meanStdDev(V_disp, srednia, odchylenieStd);
		
	// Set threshold and maxValue
	int binary_thresh;
	double maxValue = 255;
	double trs = odchylenieStd.val[0] * 9;
	binary_thresh = (int)trs;

	// Create a window
	namedWindow("V disp_bin", WINDOW_AUTOSIZE);

	Mat vDispBin;

	//-----------------------------------------------------------------------------------
	//----------------------------------   function trackbar  ---------------------------
	//-----------------------------------------------------------------------------------


	//int key = 0;
	//while (key != 27) //esc
	//{
		threshold(V_disp, vDispBin, binary_thresh, maxValue, THRESH_BINARY);
		imshow("V disp_bin", vDispBin);
	//	key = waitKey(1);
	////	imwrite(nazwa2, dst, compression_params);
	//}

	//-----------------------------------------------------------------------------------
	//-----------------------------------------------------------------------------------
	
		HoughTresh thresh;
		thresh.sr = 50;
		thresh.max = 100;
		thresh.min = 10;
		
		cv::namedWindow(detLinesWinName);
		Mat cdst;
		vector<Vec2f> lines, wybrana_linia;

		//-------------------------------------------------------------------------------


		cvtColor(V_disp, cdst, CV_GRAY2BGR);
		findOneLine(vDispBin, lines, wybrana_linia, thresh);

		//------------------------------------------------------------------------------
		// --- display lines -----------------------------------------------------------

		Point pt1, pt2;


		for (size_t i = 0; i < wybrana_linia.size(); i++)
				{
					float r = wybrana_linia[i][0], theta = wybrana_linia[i][1];
					if (varTheta(theta))			//warunek w funkcji (katy podane w stalych: theta_max theta_min)
					{
					double a = cos(theta), b = sin(theta);
						
							double x0 = a*r, y0 = b*r;
							pt1.x = cvRound(x0 + 1000 * (-b));
							pt1.y = cvRound(y0 + 1000 * (a));
							pt2.x = cvRound(x0 - 1000 * (-b));
							pt2.y = cvRound(y0 - 1000 * (a));
							line(cdst, pt1, pt2, Scalar(0, 255, 145), 1, CV_AA);
					}
				}

				imshow("V disp_bin", vDispBin);
				imshow(detLinesWinName, cdst);

				nazwa2 = "V_bin_" + nazwa;
				imwrite(nazwa2, vDispBin, compression_params);
				nazwa2 = "V_line_detect_" + nazwa;
				imwrite(nazwa2, cdst, compression_params);

	//-------------------------------------------------------------------------------

				double pixel;

				Mat ground = Mat::zeros(n_row, n_col, CV_8UC1);

				double r = wybrana_linia[0][0], th = wybrana_linia[0][1];
				double wspA = (-1) / tan(th);
				double wspB = r / sin(th);

				
				for (int row = 0; row < n_row; row++) {
					pixel = (row*1.0 - wspB)/wspA;					//wartosc piksela jaka powinno miec podloze na danym 'row'
					for (int col = 0; col < n_col; col++) {
						if (image.at<uchar>(row, col)>pixel-toleranceDisp && image.at<uchar>(row, col)<pixel + toleranceDisp)
						{
							ground.at<uchar>(row, col) = 255;
						}
					}
				}

				namedWindow("nowe okno", WINDOW_AUTOSIZE);
				imshow("nowe okno", ground);
				nazwa2 = "ground_detect_" + nazwa;
				imwrite(nazwa2, ground, compression_params);
				
	waitKey(0);         // Wait for a keystroke in the window
	return 0;
}



//---------------------------------------------------------------------
// funkcja znajdujaca tylko jeda linie przy pomocy transformaty Hougha 
//---------------------------------------------------------------------

void findOneLine(Mat vDispImage, vector<Vec2f> &linie, vector<Vec2f> &wybrane_linie, HoughTresh &thresh )
{
	wybrane_linie.clear();
	linie.clear();

	HoughLines(vDispImage, linie, rho, thetaHough, thresh.sr, 0, 0);
	
	for (size_t i = 0; i < linie.size(); i++)
	{
		if (varTheta(linie[i][1])) wybrane_linie.push_back(linie[i]);
		if (wybrane_linie.size() > 5) break;
	}

	switch (wybrane_linie.size())
	{
	case 1:  break;
	case 0: 
		thresh.max = thresh.sr;
		thresh.sr = (thresh.max - thresh.min) / 2 + thresh.min;
		if (thresh.min == thresh.sr)
			{
			cout << "Nie znaleziono lini";
			Vec2f parametry(0, 0);
			wybrane_linie.push_back(parametry);
			}
		else findOneLine(vDispImage, linie, wybrane_linie, thresh);
		break;
		
	default:
		thresh.min = thresh.sr;
		thresh.sr = thresh.max;
		thresh.max = thresh.max * 2;
		findOneLine(vDispImage, linie, wybrane_linie, thresh);
		break;
	}
}

//---------------------------------------------------------------------



//---------------------------------------------------------------------
//----    funkcja pobieraj�ca tylko po�ow� obrazu z plik�w ZED     ----
//---------------------------------------------------------------------

void checkFile(Mat &obraz, int& rows, int& cols )
{
	//------------------------------------   Pobranie tylko po�owy obrazu
	rows = obraz.rows;
	cols = obraz.cols;

	if (cols == 2560 && rows == 720)
	{
		cols = cols / 2;
		CvRect rect = cvRect(cols, 0, cols, rows);  //cvRect(int x,y, width, height)
		Mat kawalek_obrazu = obraz(rect);
		obraz = kawalek_obrazu.clone();
	}
	//-------------------------------------------------------------------
}


//---------------------------------------------------------------------
//----   funkcja warunkowa - ograniczenie szukania k�ta prostych   ----
//---------------------------------------------------------------------

bool varTheta(const double &theta)
{
	if ((theta > theta_min) && (theta < theta_max)) return true;
	else return false;
}

//---------------------------------------------------------------------